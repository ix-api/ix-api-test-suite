
"""
Test token decoding
"""

from datetime import datetime, timedelta

import jwt

from ixapi_client.v1.tokens import ApiToken


TEST_TOKEN_SECRET = "my__secret__secret"


def _make_token():
    """Make a JWT"""
    now = datetime.utcnow()
    payload = {
        "iat": now,
        "exp": now + timedelta(minutes=5),
    }

    return jwt.encode(payload, TEST_TOKEN_SECRET, algorithm="HS256")


def test_token_from_jwt():
    """Test ApiToken from jwt initialization"""
    jwt_payload = _make_token()
    token = ApiToken.from_jwt(jwt_payload)
    assert token, "Token should be loaded without errors"


def test_token_ttl():
    jwt_payload = _make_token()
    token = ApiToken.from_jwt(jwt_payload)

    # Should be 299.xxxx seconds for 5 minutes lifetime
    assert token.ttl > 299 and token.ttl < 301, \
        "Token lifetime should be the assigned 5 minutes"

