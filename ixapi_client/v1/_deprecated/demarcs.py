

"""
Demarcs Helper
"""

import time
import random

def get_demarc_by_id(session, demarc_id):
    """Retrieve the demarc"""
    result, demarc = session.get("/v1/demarcs/{}".format(demarc_id))
    assert result.ok

    return demarc


def allocate_demarc(session, customer, pop, capability, implementation_contact):
    """
    Allocate a port demarcation point with full
    authorization.
    """
    port_type = capability["type"]

    print(("Allocating: {port_type} at {pop_name} in "
           "{pop_facility}").format(
               port_type=port_type,
               pop_name=pop["name"],
               pop_facility=pop["physical_facility"]))

    demarc_input = {
        "type": port_type,
        "pop": pop["id"],
        "implementation_contacts": [implementation_contact["id"]],
        "billing_customer": customer["id"],
        "owning_customer": customer["id"],
    }

    result, demarc = session.post(
        "/v1/demarcs",
        payload=demarc_input)
    assert result.status_code == 201

    print(" ** DEMARC: {}".format(demarc["name"]))

    return demarc


def await_demarc_state(state, session, demarc_id, interval=0.5, max_tries=20):
    """Check the demarc state and wait until production"""
    demarc = get_demarc_by_id(session, demarc_id)
    if demarc["state"] == state:
        print(" ** DEMARC STATE: {}".format(state.upper()))
        return

    print(" ** DEMARC STATE: {}, AWAIT: {}".format(
        demarc["state"].upper(),
        state.upper()))

    while max_tries > 0:
        demarc = get_demarc_by_id(session, demarc_id)

        if demarc["state"] == state:
            print(" ** DEMARC STATE: {}".format(state.upper()))
            return

        time.sleep(interval)
        max_tries -= 1

    raise Exception("DEMARC state never reached: {}".format(state.upper()))

