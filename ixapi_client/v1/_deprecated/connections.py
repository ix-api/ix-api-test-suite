
"""
Connections Helper
"""

from jea import (
    authorization,
    pops,
    contacts,
    demarcs as demarcs_svc,
)

def create_lacp_connection(session, demarcs, implementation_contact):
    """
    Create an LACP connection with a list of given demarcs
    """
    customer_id = demarcs[0]["owning_customer"]
    connection_input = {
        "demarcs": [d["id"] for d in demarcs],
        "mode": "lacp",
        "lacp_timeout": "slow",
        "implementation_contacts": [implementation_contact["id"]],
        "owning_customer": customer_id,
        "billing_customer": customer_id,
    }

    res, conn = session.post("/v1/connections", connection_input)

    assert res.ok
    assert res.status_code == 201

    return conn


def create_connection(session, customer, num_demarcs=1):
    """
    Create a fully configured conenction,
    with a demarc.
    """
    # We need an allocated demarc
    pop = pops.first_pop(session)
    capability = pops.first_capability(session ,pop)
    implementation_contact = contacts.first_implementation_contact(
        session, customer)

    # Allocate demarc
    demarcs = []
    for _ in range(num_demarcs):
        print("Allocating demarc")
        demarc = demarcs_svc.allocate_demarc(
            session,
            customer,
            pop,
            capability,
            implementation_contact)
        assert demarc

        demarcs_svc.await_demarc_state(
            "allocated", session, demarc["id"])

        demarcs.append(demarc)

    # Great. Now let's create a connection
    print("Creating connection:")
    for demarc in demarcs:
        print(" - Using demarc id: {}".format(demarc["id"]))


    conn = create_lacp_connection(
        session,
        demarcs,
        implementation_contact)

    return conn

