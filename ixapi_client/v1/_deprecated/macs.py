
import secrets


def generate_mac_address():
    """Just generate a mac address"""
    mac = [secrets.token_hex(1) for _ in range(6)]
    return ":".join(mac)


def assign_mac_address(
        session, network_service_config, mac
    ):
    """Assign mac address to network service config"""
    raise NotImplemented

