
"""
Contacts Helpers
"""

def first_implementation_contact(session, customer):
    """Get any implementation contact from this customer"""
    resource = "/v1/contacts?type=implementation&owning_customer={}" \
        .format(customer["id"])
    res, contacts = session.get(resource)
    assert res.ok
    if not contacts:
        raise Exception("Need an implementation to proceed")

    return contacts[0]

