
"""
POPs helpers
"""


def first_pop(session):
    """Just get any pop"""
    res, pops = session.get("/v1/pops")
    assert res.ok

    return pops[0]

def first_capability(session, pop):
    """Get any capa from demarc"""
    pop_device = pop["available_devices"][0]
    device_id = pop_device["id"]

    # resolve device
    res, device = session.get(f"/v1/devices/{device_id}")
    assert res.ok

    return device["capabilities"][0]
