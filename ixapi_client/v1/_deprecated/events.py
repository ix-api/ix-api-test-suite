
from pprint import pprint

def dump_event_stream(events):
    """Dump an event stream"""
    try:
        for event in events:
            print("{} - {} for customer_id: {}".format(
                event["timestamp"],
                event["type"],
                event["customer"]))
            pprint(event["payload"])
            print("")
    except KeyboardInterrupt:
        print("Thank you for subscribing to cat facts")
        return

