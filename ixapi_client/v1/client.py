
"""
JEA Client
"""

import json
import time
from typing import Union, Optional
from functools import wraps

import requests

from ixapi_client.v1 import exceptions
from ixapi_client.v1.tokens import ApiToken
from ixapi_client.v1.resources import (
    auth,
    service,
    crm,
)


AUTH_REFRESH = "/v1/refresh"
AUTH_TOKEN = "/v1/token"


def unpack_response(response):
    """
    Decode json body and return result
    and response as a tuple.

    :param response: A requests response object
    """
    try:
        payload = response.json()
    except json.JSONDecodeError:
        payload = response.text

    return (response, payload)

#
# Client
#
class Client:
    """
    An authenticated JEA client session
    """
    def __init__(
            self,
            host: str,
            access_token: ApiToken = None,
            refresh_token: ApiToken = None,
        ):
        """
        Initialize the session
        """
        self.host = host
        self.access_token = access_token
        self.refresh_token = refresh_token


    def refresh(self, update=False):
        """
        Refresh session.
        Get new access and refresh token from the backend by sending
        the refresh token.

        :param update: Create a new session or update the current object.
        """
        access_token, refresh_token = auth.auth_token_refresh(self, {
            "refresh_token": str(self.refresh_token),
        })

        if update:
            # Update our own session instead of creating a fresh one
            self.access_token = access_token
            self.refresh_token = refresh_token

            return self

        return Client(self.host, access_token, refresh_token)

    def _get_request_headers(self):
        """
        Get headers for the request. This includes
        authentication related headers.
        """
        if not self.access_token:
            return {}

        # Make berare header
        return {
            "Authorization": "Bearer {}".format(self.access_token)
        }

    def get(self, resource, query=None, resolve_refs=False):
        """Wrap requests GET"""
        res = requests.get(
            self.host + str(resource), query,
            headers=self._get_request_headers())

        return unpack_response(res)

    def post(self, resource, payload=None, resolve_refs=False):
        """Wrap requsts POST"""
        res = requests.post(
            self.host + str(resource), json=payload,
            headers=self._get_request_headers())

        return unpack_response(res)

    def put(self, resource, payload=None, resolve_refs=False):
        """Wrap requests PUT"""
        res = requests.put(
            self.host + str(resource), json=payload,
            headers=self._get_request_headers())

        return unpack_response(res)

    def patch(self, resource, payload=None, resolve_refs=False):
        """Wrap requests PATCH"""
        res = requests.patch(
            self.host + str(resource), json=payload,
            headers=self._get_request_headers())

        return unpack_response(res)

    def delete(self, resource, resolve_refs=False):
        """Wrap requests DELETE"""
        res = requests.delete(
            self.host + str(resource),
            headers=self._get_request_headers())

        return unpack_response(res)

    def options(self, resource, resolve_refs=False):
        """Get resource OPTIONS"""
        res = requests.options(
            self.host + str(resource),
            headers=self._get_request_headers())

        return unpack_response(res)



def dial(host, api_key, api_secret) -> Client:
    """
    Initialize an authenticated session with the server.
    """
    client = Client(host)
    access_token, refresh_token = auth.auth_token_create(client, {
        "api_key": api_key,
        "api_secret": api_secret,
    })
    # Create authorized client
    client = Client(host, access_token, refresh_token)

    return client

