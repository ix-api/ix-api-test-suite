
"""
Access Resources
"""

from ixapi_client.v1.resource import (
    status_check,
    validate_response,
    api_get,
    api_post,
    api_update,
    api_delete,
)


## Demarcs

@validate_response
@status_check
@api_get
def demarcs_list():
    """List demarcs"""
    return "/demarcs"


@validate_response
@status_check
@api_get
def demarcs_retrieve(pk):
    """Get demarc"""
    return f"/demarcs/{pk}"


@validate_response
@status_check
@api_update
def demarcs_update(pk, update):
    """Update demarc"""
    return f"/demarcs/{pk}", update


## Connections 

@validate_response
@status_check
@api_get
def connections_list():
    """List connections"""
    return "/connections"


@validate_response
@status_check
@api_get
def connections_retrieve(pk):
    """Get a connection"""
    return f"/connections/{pk}"


@validate_response
@status_check
@api_post
def conncetion_create(request):
    """Create a connection"""
    return "/connections", request


@validate_response
@status_check
@api_update
def connection_update(pk, update):
    """Update a connection"""
    return f"/connections/{pk}", update


@validate_response
@status_check
@api_delete
def connection_destroy(pk):
    """Delete a connection"""
    return f"/connections/{pk}"


## Network Service Configs

@validate_response
@status_check
@api_get
def network_service_configs_list():
    """List network service config"""
    return "/network-service-configs"


@validate_response
@status_check
@api_get
def network_service_configs_retrieve(pk):
    """Get a network service config"""
    return f"/network-service-configs/{pk}"


@validate_response
@status_check
@api_post
def network_service_configs_create(request):
    """Create a network service configuration"""
    return "/network-service-configs", request


@validate_response
@status_check
@api_update
def network_service_configs_update(pk, update):
    """Update a network service config"""
    return f"/network-service-configs/{pk}", update


@validate_response
@status_check
@api_delete
def network_service_configs_destroy(pk):
    """Remove a network service config"""
    return f"/network-service-configs/{pk}"


## Network Feature Configs

@validate_response
@status_check
@api_get
def network_feature_configs_list():
    """List network feature config"""
    return "/network-feature-configs"


@validate_response
@status_check
@api_get
def network_feature_configs_retrieve(pk):
    """Get a network feature config"""
    return f"/network-feature-configs/{pk}"


@validate_response
@status_check
@api_post
def network_feature_configs_create(request):
    """Create a network feature configuration"""
    return "/network-feature-configs", request


@validate_response
@status_check
@api_update
def network_feature_configs_update(pk, update):
    """Update a network feature config"""
    return f"/network-feature-configs/{pk}", update


@validate_response
@status_check
@api_delete
def network_feature_configs_destroy(pk):
    """Remove a network feature config"""
    return f"/network-feature-configs/{pk}"

