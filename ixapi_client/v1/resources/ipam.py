
"""
IPAM Resources
"""

from ixapi_client.v1.resource import (
    status_check,
    validate_response,
    api_get,
    api_post,
    api_update,
    api_delete,
)

## Ip Addresses

@validate_response
@status_check
@api_get
def ips_list():
    """List ip addresses"""
    return "/ips"


@validate_response
@status_check
@api_get
def ips_retrieve(pk):
    """Get an ip address by id"""
    return f"/ips/{pk}"


@validate_response
@status_check
@api_post
def ips_create(request):
    """Create an ip address record"""
    return "/ips", request


@validate_response
@status_check
@api_update
def ips_update(pk, update):
    """Update an ip address"""
    return f"/ips/{pk}", update


@validate_response
@status_check
@api_delete
def ips_destroy(pk):
    """Remove an ip address"""
    return f"/ips/{pk}"



## Mac Addresses

@validate_response
@status_check
@api_get
def macs_list():
    """List mac addresses"""
    return "/macs"


@validate_response
@status_check
@api_get
def macs_retrieve(pk):
    """Get a mac address by id"""
    return f"/macs/{pk}"


@validate_response
@status_check
@api_post
def macs_create(request):
    """Create a mac address record"""
    return "/macs", request


@validate_response
@status_check
@api_update
def macs_update(pk, update):
    """Update a mac address"""
    return f"/macs/{pk}", update


@validate_response
@status_check
@api_delete
def macs_destroy(pk):
    """Remove a mac address"""
    return f"/macs/{pk}"

