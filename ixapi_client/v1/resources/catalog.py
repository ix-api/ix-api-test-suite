
"""
Catalog Resources
"""

from ixapi_client.v1.resource import (
    status_check,
    validate_response,
    api_get,
)


## Devices

@validate_response
@status_check
@api_get
def devices_list():
    """Get all devices"""
    return "/devices"


@validate_response
@status_check
@api_get
def devices_retrieve(pk):
    """Get a device by id"""
    return f"/devices/{pk}"


## Pops

@validate_response
@status_check
@api_get
def pops_list():
    """Get all pops"""
    return "/pops"


@validate_response
@status_check
@api_get
def pops_retrieve(pk):
    """Get a pop by id"""
    return f"/pops/{pk}"


## Facilities

@validate_response
@status_check
@api_get
def facilities_list():
    """Get all facilities"""
    return "/facilities"


@validate_response
@status_check
@api_get
def facilities_retrieve(pk):
    """Get a facility by id"""
    return f"/facilities/{pk}"


## Products

@validate_response
@status_check
@api_get
def products_list():
    """Get all products"""
    return "/products"


@validate_response
@status_check
@api_get
def products_retrieve(pk):
    """Get a product by id"""
    return f"/products/{pk}"


