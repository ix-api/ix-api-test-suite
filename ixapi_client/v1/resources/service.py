
"""
Access Resources
"""

from ixapi_client.v1.resource import (
    status_check,
    validate_response,
    api_get,
)


## Network Services

@validate_response
@status_check
@api_get
def network_services_list():
    """List all network services"""
    return "/network-services"


@validate_response
@status_check
@api_get
def network_services_retrieve(pk):
    """Get a network service by id"""
    return f"/network-services/{pk}"


## Network Features

@validate_response
@status_check
@api_get
def network_features_list():
    """List all network features"""
    return "/network-features"


@validate_response
@status_check
@api_get
def network_features_retrieve(pk):
    """Get a network service by id"""
    return f"/network-features/{pk}"



