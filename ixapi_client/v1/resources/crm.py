
"""
CRM Resources
"""

from ixapi_client.v1.resource import (
    status_check,
    validate_response,
    api_get,
    api_post,
    api_update,
    api_delete,
)


@validate_response
@status_check
@api_get
def customers_list():
    """Get all customers"""
    return "/customers"


@validate_response
@status_check
@api_get
def customers_retrieve(pk):
    """Get customer by pk"""
    return f"/customers/{pk}"


@validate_response
@status_check
@api_post
def customers_create(request):
    """Create a customer"""
    return "/customers", request


@validate_response
@status_check
@api_update
def customers_update(pk, request):
    """Update customer"""
    return f"/customers/{pk}", request


@validate_response
@status_check
@api_get
def contacts_list():
    """get all contacts"""
    return "/contacts"


@validate_response
@status_check
@api_get
def contacts_retrieve(pk):
    """Get a contact"""
    return f"/contacts/{pk}"


@validate_response
@status_check
@api_post
def contacts_create(request):
    """Create a contact"""
    return "/contacts", request


@validate_response
@status_check
@api_update
def contacts_update(pk, request):
    """Update a contact"""
    return f"/contacts/{pk}", request

