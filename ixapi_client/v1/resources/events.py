

import time

from ixapi_client.v1.resource import api_get, status_check


@status_check
@api_get
def events_list(after=0):
    """Get events in the session"""
    return f"/events?after={after}"


def subscribe_events(session):
    """Produce a stream of events"""
    last_serial = 0
    # Fast forward
    while True:
        _, events = events_list(session, last_serial)
        if not events:
            break

        last_event = events[-1]
        last_serial = last_event["serial"]

    while True:
        _, events = events_list(session, last_serial)
        if not events:
            time.sleep(1)

        for event in events:
            last_serial = event["serial"]
            yield event

