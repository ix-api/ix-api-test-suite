
"""
Spec based validators using Cerberus as validation framework.
"""

import mergedeep
import cerberus
import pprint


class ValidationError(ValueError):
    pass

class PropertyTypeHandlerMissing(Exception):
    pass

class IncompatibleSchemasError(ValueError):
    pass

#
# Validator Class
#
def _prepare_schema(schema) -> dict:
    """
    Since cerberus does not validate top level list objects,
    we wrap our list schema in a toplevel dict object.

    Also, toplevel dict schemata want to be unpacked
    and the inner schema, describing the actual document.
    This is not the case for lists.

    :param schema: The document schema to validate.
    """
    if not schema:
        return schema

    # Unpack toplevelschema if present and use as response
    if schema.get("check_with"):
        # This is an ugly hack. I tried to make it more
        # elegant. *sigh*
        return {
            "__document__": {
                "check_with": schema["check_with"],
            },
        }

    if schema.get("schema"):
        return {
            "__document__": schema,
        }

    # Otherwise just wrap schema in toplevel object response.
    return {
        "__document__": {
            "schema": schema,
        },
    }


def _prepare_document(document) -> dict:
    """
    Since cerberus does not validate top level list objects,
    we wrap our document in case it is a list.

    :param document: The document to validate.
    """
    return {
        "__document__": document,
    }


class Validator:
    """
    Cerberus validator wrapper, to enable support for
    toplevel lists and polymorphism.
    """
    def __init__(self, schema):
        """
        Initialize validator wrapper.

        To support top level list documents, we wrap
        the list schema in a dict.

        :param schema: A validation schema
        """
        self.schema = _prepare_schema(schema)

        self.validator = cerberus.Validator(self.schema)
        self.errors = []


    def validate(self, document):
        """
        Validate a document. If this document is a list,
        wrap it in a toplevel dict object.

        Passing a schema is no longer supported.

        :param document: The document to validate
        """
        document = _prepare_document(document)
        result = self.validator.validate(document)
        self.errors = self.validator.errors

        return result


#
# Schema fragments
#
def _frag_nullable(schema, prop):
    """Make schema fragment nullable on demand"""
    if prop.get("nullable"):
        schema["nullable"] = True

    return schema


def _frag_min_integer(schema, prop):
    """Make minimum integer validation"""
    minimum = prop.get("minimum")
    if isinstance(minimum, int):
        schema["min"] = minimum

    return schema


def _frag_max_integer(schema, prop):
    """Make max integer validation"""
    maximum = prop.get("maximum")
    if isinstance(maximum, int):
        schema["max"] = maximum

    return schema


def _frag_min_length(schema, prop):
    """Make minimum length validation"""
    minimum = prop.get("minLength")
    if isinstance(minimum, int):
        schema["minlength"] = minimum

    return schema


def _frag_max_length(schema, prop):
    """Make max length validation"""
    maximum = prop.get("maxLength")
    if isinstance(maximum, int):
        schema["maxlength"] = maximum

    return schema


def _frag_string_format_date(schema, _prop):
    """Create a date format pattern"""
    regex = (r"^(\d+)-(0[1-9]|1[012])-(0[1-9]|[12]\d|3[01])$")
    schema["regex"] = regex
    return schema


def _frag_string_format_date_time(schema, _prop):
    """Create a date time format pattern"""
    regex = (r"^(\d+)-(0[1-9]|1[012])-(0[1-9]|[12]\d|3[01])"
             r"[\s|T]([01]\d|2[0-3]):([0-5]\d):([0-5]\d|60)(\.\d+)?"
             r"(([Zz])|([\+|\-]([01]\d|2[0-3])))$")
    schema["regex"] = regex
    return schema


def _frag_string_format_email(schema, _prop):
    """Create email checking pattern"""
    regex = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

    schema["regex"] = regex
    return schema


def _frag_string_format_byte(schema, _prop):
    """Accept only base64"""
    regex = r"[a-zA-Z0-9+=]*"
    schema["regex"] = regex
    return schema


def _frag_string_format_pattern(schema, prop):
    """Make validation schema from string format"""
    fmt = prop.get("format")
    if not fmt:
        return schema

    formats = {
        "date": _frag_string_format_date,
        "date-time": _frag_string_format_date_time,
        "byte": _frag_string_format_byte,
        "email": _frag_string_format_email,
    }

    make_format = formats.get(fmt)
    if not make_format:
        return schema # Nothing to do here

    return make_format(schema, prop)


def _frag_string_pattern(schema, prop):
    """Get regexpattern"""
    regex = prop.get("pattern")
    if not regex:
        return schema

    schema["regex"] = regex

    return schema

#
# Schema helpers
#

def _merge_specs(props):
    return mergedeep.merge({}, *props)

#
# Custom validators
#
def _polymorphic_any_of_schema_validator(schemata, required=False):
    """Create a polymorphic schema validator"""
    def any_of_validator(field, value, error):
        """Validate input, check if anything matches"""
        if required and value is None:
            error(field, "Input is required but is None")
            return

        errors = {}
        for i, schema in enumerate(schemata):
            validator = cerberus.Validator(schema)
            if validator.validate(value):
                # Okay something was OK let's finish here
                return
            else:
                # Save error
                errors["Schema #{}".format(i+1)] = validator.errors

        # We have come here without success
        message = ("Input did not match any of the provided schemata.\n"
                   "Errors: {}").format(errors)

        error(field, message)

    return any_of_validator

# For now keep this as an alias:
def _polymorphic_one_of_schema_validator(
        mapping_key,
        schemata,
        required=False,
    ):
    """Create a polymorphic schema validator"""
    def one_of_validator(field, value, error):
        """Validate input, check if anything matches"""
        if required and value is None:
            error(field, "Input is required but is None")
            return

        mapping_type = value[mapping_key]
        schema = schemata.get(mapping_type)
        if not schema:
            print("WARNING: Missing schema for polymorphic type: {}".format(
                mapping_type))
            return # Well. There is a schema missing. Don't validate

        errors = {}
        validator = cerberus.Validator(schema)
        if validator.validate(value):
            return

        # We have come here without success
        message = ("Input did not match any of the provided schemata.\n"
                   "Errors: {}").format(validator.errors)

        error(field, message)

    return one_of_validator

#
# Validator schemas from properties:
#
def _schema_from_prop_integer(root, prop: dict, required=False) -> dict:
    """
    Derive schema from integer property

    :param prop: The property specs
    :param required: Set the required flag
    :return: A cerberus validation schema for integer fields
    """
    schema = {
        "type": "integer",
        "required": required,
    }

    # Set schema properties
    schema = _frag_min_integer(schema, prop)
    schema = _frag_max_integer(schema, prop)
    schema = _frag_nullable(schema, prop)

    return schema


def _schema_from_prop_boolean(root, prop: dict, required=False) -> dict:
    """
    Derive a schema for validating boolean properties

    :param prop: The property specs
    :param required: Set the required flag
    :return: A cerberus validation schema for boolean fields
    """
    schema = {
        "type": "boolean",
        "required": required,
    }

    schema = _frag_nullable(schema, prop)
    return schema


def _schema_from_prop_string(root, prop: dict, required=False) -> dict:
    """
    Derive string validation schema.
    Perform content checks if a format is given.

    :param prop: The property specs
    :param required: Set the required flag
    :return: A cerberus validation schema for string fields
    """
    schema = {
        "type": "string",
        "required": required,
    }

    # Set schema properties
    schema = _frag_nullable(schema, prop)
    schema = _frag_min_length(schema, prop)
    schema = _frag_max_length(schema, prop)

    # Handle format checks
    schema = _frag_string_format_pattern(schema, prop)
    schema = _frag_string_pattern(schema, prop)

    return schema


def _resolve_ref(spec, ref):
    """Get a referenced schema"""
    if ref.startswith("#"):
        ref = ref[1:]
    path = ref.split("/")[1:]
    for key in path:
        spec = spec[key]
    return spec


def _schema_from_prop_object(root, prop: dict, required=False) -> dict:
    """
    Derive a schema for an object property.

    :param prop: The property specs
    :param required: Set the required flag
    """
    # Apply allOf
    all_of = prop.get("allOf", [])
    if all_of:
        merged = _merge_specs(all_of)
        schema = schema_from_prop(root, merged)
        return schema

    # Apply anyOf
    any_of = prop.get("anyOf", [])
    if any_of:
        any_of_schemata = [schema_from_prop(root, p)["schema"] for p in any_of]
        validator = _polymorphic_any_of_schema_validator(any_of_schemata)
        schema = {
            "check_with": validator,
        }
        return schema

    # Apply oneOf
    one_of = prop.get("oneOf", [])
    if one_of:
        discriminator = prop["discriminator"]
        one_of_key = discriminator["propertyName"]
        one_of_schemata = {
            mapping_type: schema_from_prop(
                root, _resolve_ref(root, ref))["schema"]
            for mapping_type, ref in discriminator["mapping"].items()
        }
        validator = _polymorphic_one_of_schema_validator(
            one_of_key, one_of_schemata)
        schema = {
            "check_with": validator,
        }
        return schema

    # Otherwise: Make properties schema
    properties = prop.get("properties", {})
    required_properties = prop.get("required", [])
    properties_schema = {
        name: schema_from_prop(
            root, prop, required=(name in required_properties))
        for name, prop in properties.items()
    }

    # In case properties are empty, we allow for unknown fields
    allow_unknown = False
    if not properties:
        allow_unknown = True

    schema = {
        "type": "dict",
        "required": required,
        "schema": properties_schema,
        "allow_unknown": allow_unknown,
    }

    schema = _frag_nullable(schema, prop)

    return schema


def _schema_from_prop_array(root, prop: dict, required=False) -> dict:
    """
    Derive a schema for an array property.

    :param prop: The property specs
    :param required: Set the required flag
    """
    # We want to validate a list.
    items_prop = prop.get("items", {})
    items_schema = schema_from_prop(root, items_prop)

    schema = {
        "type": "list",
        "schema": items_schema,
    }

    return schema


def schema_from_prop(root, prop: dict, required=False) -> dict:
    """
    Derive schema from property
    """
    handlers = {
        "integer": _schema_from_prop_integer,
        "string": _schema_from_prop_string,
        "object": _schema_from_prop_object,
        "array": _schema_from_prop_array,
        "boolean": _schema_from_prop_boolean,
    }

    # Assume object if no type is given
    prop_type = prop.get("type", "object")
    if not prop_type:
        return {}

    try:
        return handlers[prop_type](root, prop, required)
    except KeyError:
        raise PropertyTypeHandlerMissing(prop_type)


def schema_from_spec(spec: dict) -> dict:
    """Wrapper to exclude root object"""
    schema = schema_from_prop(spec, spec)
    return schema["schema"]


