
"""
OpenAPI spec handling tests
"""

import pytest

from ixapi_client.spec import openapi


SPEC_FILE = "schema/ix-api-schema-v1.json"


def test_load_spec():
    """Test loading the OAS3 YAML"""
    specs = openapi.load_spec(SPEC_FILE)

    assert specs, \
        "Specs should load without error"

    assert isinstance(specs, dict), \
        "Specs should be a plain dict"


def test_get_path():
    """Test resolving a path in the spec"""
    spec = openapi.load_spec(SPEC_FILE)

    subset = openapi.get_path(spec, "paths./auth/token.post.responses.200")

    assert subset, \
        "Subset should be resolved without error"
    assert isinstance(subset, dict), \
        "Subset should be a plain dict"

    # Negative test
    subset = openapi.get_path(spec, "aaaaaaaaaaaaaaa", 42)

    assert subset == 42, \
        "When a path can not be resolved, the default should be returned"


def test_get_response():
    """Test response object resolving"""
    spec = openapi.load_spec(SPEC_FILE)

    # A valid token response
    response_spec = openapi.get_response(spec, "/auth/token", "post", 200)

    assert response_spec, \
        "Response spec should be resolved without error"
    assert isinstance(response_spec, dict), \
        "Response spec should be a plain dict"

    # Missing response
    with pytest.raises(openapi.SpecNotFound):
        openapi.get_response(spec, "AAAAAAAAAAAA", "post", 23)


def test_get_response_validator__collection_response():
    """
    Test getting a validator from the spec,
    for a collection endpoint.
    """
    spec = openapi.load_spec(SPEC_FILE)

    # A valid token response
    validator = openapi.get_response_validator(
        spec, "/customers", "get", 200)

    assert validator, \
        "Validator should be created without error"


def test_get_response_validator__item_response():
    """
    Test getting a validator from the spec for
    a single item response.
    """
    spec = openapi.load_spec(SPEC_FILE)

    # A valid token response
    validator = openapi.get_response_validator(
        spec, "/customers/{id}", "get", 200)

    assert validator, \
        "Validator should be created without error"


def test_load_response_schemata():
    """Test getting the validator for all responses"""
    spec = openapi.load_spec(SPEC_FILE)

    for path, path_spec in spec["paths"].items():
        print("Loading validator for: {}".format(path))
        methods = [method for method in path_spec
                   if method in [
                       'get', 'post', 'put',
                       'delete', 'patch', 'options']]

        for method in methods:
            print(" > Method: {}".format(method))
            method_spec = path_spec[method]
            # Get responses
            for status_code in method_spec["responses"]:
                print(" - {}: {}".format(method, status_code))
                openapi.get_response_validator(
                    spec, path, method, status_code)



