
"""
Test validators
"""

from ixapi_client.spec import validators


# A simple spec
AUTH_TOKEN_SPEC = {
    "type": "object",
    "properties": {
        "result": {
            "title": "An auth token pair",
            "type": "object",
            "required": [
                "access_token",
                "refresh_token",
            ],
            "properties": {
                "access_token": {
                    "type": "string",
                    "example":
                    "ey0XiiK1iChGiiIzI1NiJ9.eyJp...",
                },
                "refresh_token": {
                    "type": "string",
                    "example": "eyJ0eXAiOiJKV1QeyJp...",
                },
            },
        },
    },
}

RESPONSE_SPEC = {
    "title": "Response",
    "type": "object",
    "properties": {
        "result": {
            "type": "object",
            "nullable": True,
        },
        "status": {
            "title": "Status",
            "type": "object",
            "required": [
                "code",
                "tag",
                "created_at",
            ],
            "properties": {
                "code": {
                    "type": "integer",
                    "description": "A status code",
                    "example": 200,
                },
                "tag": {
                    "type": "string",
                    "description": "A status group mnemonic",
                    "example": "resource_error"
                },
                "error": {
                    "type": "object",
                    "description": "An error object",
                    "nullable": True
                },
                "created_at": {
                    "type": "string",
                    "description": "When the response was created",
                    "format": "date-time",
                },
            },
        },
    },
}


FULL_RESPONSE_SPEC = {
    "allOf": [
        RESPONSE_SPEC,
        AUTH_TOKEN_SPEC,
    ],
}


ARRAY_SPEC = {
    "type": "array",
    "items": {
        "type": "object",
        "required": {
            "foo",
        },
        "properties": {
            "foo": {
                "type": "string",
            },
            "bar": {
                "type": "integer",
            },
        },
    },
}


def test__schema_for_prop_integer():
    """Test integer property to schema"""
    schema = {
        "var": validators._schema_from_prop_integer(None, {
            "type": "integer",
            "minimum": 2,
        }, required=True),
    }

    v = validators.Validator(schema)

    assert v.validate({"var": 23}), v.errors
    assert not v.validate({"var": 1})
    assert not v.validate({"foo": None})

    # Check nullable
    schema = {
        "var": validators._schema_from_prop_integer(None, {
            "type": "integer",
            "nullable": True
        }, required=True),
    }

    v = validators.Validator(schema)
    assert v.validate({"var": None}), v.errors
    assert not v.validate({})

    # Check required
    schema = {
        "var": validators._schema_from_prop_integer(None, {
            "type": "integer",
        }, required=False),
    }
    v = validators.Validator(schema)
    assert v.validate({}), v.errors


def test__schema_for_prop_string():
    """Test string validation"""
    schema = {
        "var": validators._schema_from_prop_string(None, {
            "type": "string",
            "minLength": 2,
            "maxLength": 4,
        }, required=True),
    }
    v = validators.Validator(schema)

    assert v.validate({"var": "abc"}), v.errors
    assert not v.validate({"var": "a"})
    assert not v.validate({"var": "abcde"})


def test__schema_for_prop_string_formats_date_time():
    """Test string validation"""
    schema = {
        "var": validators._schema_from_prop_string(None, {
            "type": "string",
            "format": "date-time",
        }, required=True),
    }
    v = validators.Validator(schema)

    assert v.validate({"var": "2019-01-02T20:23:00.2312Z"}), v.errors
    assert v.validate({"var": "2019-01-02 20:23:00.2312Z"}), v.errors
    assert v.validate({"var": "2019-01-02 20:23:00Z"}), v.errors
    assert not v.validate({"var": "a"})
    assert not v.validate({"var": "abcde"})


def test__schema_for_prop_string_formats_date():
    """Test string validation"""
    schema = {
        "var": validators._schema_from_prop_string(None, {
            "type": "string",
            "format": "date",
        }, required=True),
    }
    v = validators.Validator(schema)

    assert v.validate({"var": "2019-01-02"}), v.errors
    assert v.validate({"var": "2019-01-02"}), v.errors
    assert not v.validate({"var": "2019-01-02T20:23:00Z"}), v.errors
    assert not v.validate({"var": "a"})
    assert not v.validate({"var": "abcde"})


def test__schema_for_prop_string_formats_email():
    """Test string validation"""
    schema = {
        "var": validators._schema_from_prop_string(None, {
            "type": "string",
            "format": "email",
        }, required=True),
    }
    v = validators.Validator(schema)

    assert v.validate({"var": "ben@utzer.com"}), v.errors
    assert not v.validate({"var": "a"})
    assert not v.validate({"var": "abcde.com"})


def test__schema_for_prop_string_formats_byte():
    """Test string validation"""
    schema = {
        "var": validators._schema_from_prop_string(None, {
            "type": "string",
            "format": "byte",
        }, required=True),
    }
    v = validators.Validator(schema)

    assert v.validate({"var": "ZjAwYmFyYmF6eg=="}), v.errors
    assert v.validate({"var": "YXNk"}), v.errors
    assert not v.validate({"var": "ZjAwYmFyYm_F6YmF/Rm5vcmQ="})


def test__schema_for_prop_string_pattern():
    """Test string validation"""
    schema = {
        "var": validators._schema_from_prop_string(None, {
            "type": "string",
            "pattern": "[a-f]+",
        }, required=True),
    }
    v = validators.Validator(schema)

    assert v.validate({"var": "abcdef"}), v.errors
    assert v.validate({"var": "beef"}), v.errors
    assert not v.validate({"var": "Beef"})
    assert not v.validate({"var": "beef9"})


def test__schema_for_prop():
    """Test schema handler resolving"""
    integer_prop = {"type": "integer"}
    string_prop = {"type": "string"}
    object_prop = {"type": "object"}

    schema = validators.schema_from_prop(None, integer_prop)
    assert schema["type"] == "integer"

    schema = validators.schema_from_prop(None, string_prop)
    assert schema["type"] == "string"

    schema = validators.schema_from_prop(None, object_prop)
    assert schema["type"] == "dict"


def test__schema_from_prop_object():
    """Test recursive object mapping"""
    composed_prop = {
        "type": "object",
        "properties": {
            "foo": {
                "type": "integer",
            },
            "bar": {
                "type": "object",
                "required": [
                    "baz",
                ],
                "properties": {
                    "baz": {
                        "type": "string",
                        "minLength": 3,
                    }
                }
            }
        }
    }

    # Let's do some validation
    schema = {"var": validators.schema_from_prop(None, composed_prop)}
    v = validators.Validator(schema)

    assert v.validate({
        "var": {
            "foo": 42,
            "bar": {
                "baz": "abc",
            }
        }
    }), v.errors

    # Missing fields
    assert not v.validate({
        "var": {
            "foo": 42,
            "bar": {
            }
        }
    }), "Required field must be missing from document"


def test__merge_specs():
    """Terst merging specs"""
    all_of = [
        {
            "type": "object",
            "properties": {
                "foo": {
                    "type": "integer",
                },
                "baz": {
                    "type": "string",
                },
            },
        },
        {
            "type": "object",
            "properties": {
                "bar": {
                    "type": "string",
                },
                "baz": {
                    "type": "integer"
                }
            },
        },
    ]

    merged = validators._merge_specs(all_of)
    print(merged)


def test__merge_object_schemata():
    """Test all of schema combination"""
    spec = {
        "allOf": [
            {
                "type": "object",
                "properties": {
                    "foo": {
                        "type": "integer",
                    },
                    "baz": {
                        "type": "string",
                    }
                },
            },
            {
                "type": "object",
                "properties": {
                    "bar": {
                        "type": "string",
                    },
                    "baz": {
                        "type": "integer"
                    }
                },
            },
        ]
    }

    schema = validators.schema_from_prop(spec, spec)

    assert schema["schema"]["foo"]
    assert schema["schema"]["bar"]
    assert schema["schema"]["baz"] # All should be present
    assert schema["schema"]["baz"]["type"] == "integer", \
        "Properties of the same name should override"


def test_load_full_response_spec():
    """Real example test"""
    response = {
        "result": {
            "access_token": "4CC355_T0K3N",
            "refresh_token": "R3FR35H_T0K3N",
        },
        "status": {
            "code": 200,
            "tag": "ok",
            "created_at": "2019-01-02T20:12:13Z",
        }
    }

    schema = validators.schema_from_prop(FULL_RESPONSE_SPEC, FULL_RESPONSE_SPEC)
    v = validators.Validator(schema)
    assert v.validate(response), v.errors


def test_schema_from_array_spec():
    """Generate a schema from an array spec"""
    schema = validators.schema_from_prop(None, ARRAY_SPEC)
    response = [
        {"foo": "test1", "bar": 123580},
        {"foo": "test2", "bar": 321},
    ]

    v = validators.Validator(schema)
    assert v.validate(response), v.errors


def test_any_of_polymorphism():
    """Test any of schema polymorphism"""
    spec = {
        "type": "object",
        "properties": {
            "foo": {
                "type": "object",
                "anyOf": [
                    {"type": "object",
                     "properties": { "bar": {"type": "string"}}},
                    {"type": "object",
                     "properties": { "baz": {"type": "integer"}}}
                ]
            },
        },
    }

    schema = validators.schema_from_prop(spec, spec)

    document_a = {"foo": {"bar": "case_1"}}
    document_b = {"foo": {"baz": 2}}
    document_c = {"foo": {"bar": "case_1", "baz": 3}}

    v = validators.Validator(schema)
    assert v.validate(document_a), v.errors
    assert v.validate(document_b), v.errors
    assert not v.validate(document_c)


#
# Test validators
#
def test__prepare_schema__list():
    """A list schema should be wrapped in a toplevel document"""
    schema = {
        "type": "list",
        "schema": {
            "type": "integer",
        }
    }

    wrapped_schema = validators._prepare_schema(schema)
    assert not wrapped_schema.get("type") == "list"


def test__prepare_schema__dict():
    """
    A dict toplevel schema should not be wrapped,
    however - the content is unpacked and the inner
    schema should be used for validation.
    """
    schema = {
        "type": "dict",
        "schema": {
            "foo": {
                "type": "integer",
            },
        }
    }

    validators._prepare_schema(schema)


def test__prepare_document__list():
    """A list document should be wrapped in a dict toplevel"""
    document = [{"foo": 23}]
    wrapped_document = validators._prepare_document(document)

    assert isinstance(document, list)
    assert isinstance(wrapped_document, dict)


def test__prepare_document__dict():
    """A dict document should not be wrapped"""
    document = {
        "foo": 23,
    }

    wrapped_document = validators._prepare_document(document)

    assert isinstance(wrapped_document, dict)
    assert wrapped_document != document


def test_validator__dict():
    """Test the validator class"""
    schema = {
        "schema": {
            "foo": {
                "type": "integer",
            },
        },
    }
    document = {
        "foo": 23,
    }

    validator = validators.Validator(schema)
    assert validator.validate(document), validator.errors


def test_validator__list():
    """Test the validator class: list input"""
    schema = {
        "type": "list",
        "schema": {
            "type": "dict",
            "schema": {
                "foo": {
                    "type": "integer",
                },
            },
        },
    }
    document = [
        {"foo": 23},
    ]

    validator = validators.Validator(schema)
    assert validator.validate(document), validator.errors

