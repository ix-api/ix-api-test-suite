
"""
OpenApi spec loader using prance for resolving.
"""

from prance.util.resolver import RefResolver
from prance.util.formats import parse_spec

from ixapi_client.spec import validators
from ixapi_client.spec.validators import Validator


class SpecNotFound(Exception):
    pass


def _load_file(filename):
    """
    Load open api specification and parse the result.
    """
    with open(filename) as f:
        return parse_spec(f.read(), filename=filename)


def load_spec(filename):
    """
    Load specification file and resolve references.
    """
    spec = _load_file(filename)

    resolver = RefResolver(spec, "/")
    resolver.resolve_references()

    return resolver.specs


def get_path(spec, path, default=None):
    """
    Return a subset of the set identified by the path.

    Example:
        components.schemas.my-user-input
        paths./auth/token.responses

    """
    parts = path.split(".")
    if not parts:
        return spec

    if len(parts) == 1:
        return spec.get(path, default)

    # Get next key and paths part
    key = parts[0]
    next_path = ".".join(parts[1:])

    try:
        subspec = spec[key]
    except KeyError:
        return default
    else:
        return get_path(subspec, next_path, default)


def get_response(
        spec,
        url_path,
        method,
        status,
        content_type="application/json",
    ):
    """
    Get a response object from the spec file.

    Example:
        get_response(spec, "/auth/token", "POST", 200)

    :param spec: The parsed spec
    :param url_path: The path in the 'paths' section
    :param method: The request method: GET POST PUT DELETE OPTIONS
    :param status: The response status code

    :raises: SpecNotFound when there is no spec
    """
    path = "paths.{}.{}.responses.{}.content.{}.schema".format(
        url_path, method.lower(), status, content_type)
    spec = get_path(spec, path)
    if not spec:
        raise SpecNotFound(path)

    return spec


def get_response_validator(
        spec,
        url_path,
        method,
        status,
        content_type="application/json",
    ):
    """
    Get a response validator based on the spec

    :param spec: The parsed spec
    :param url_path: The path in the 'paths' section
    :param method: The request method: GET POST PUT DELETE OPTIONS
    :parm status: The response status code
    """
    response_spec = get_response(spec, url_path, method, status, content_type)
    schema = validators.schema_from_prop(spec, response_spec)

    validator = Validator(schema)
    setattr(validator, "_original_spec", response_spec)

    return validator

