
import sys
from argparse import Namespace

import pytest
import requests

from cli.utils.config import load_json_config
from cli.utils.text import print_f
from ixapi_client.v1 import client
from ixapi_client.v1.resources import (
    crm,
    access,
    service,
)

# Global configuration
_ARGS = None
_IX_API_CONFIG = None
_SESSION = None


@pytest.fixture
def session():
    """An authenticated api session"""
    global _SESSION
    if not _SESSION:
        _SESSION = client.dial(
            host=_IX_API_CONFIG.api_base_url,
            api_key=_IX_API_CONFIG.api_key,
            api_secret=_IX_API_CONFIG.api_secret)

    return _SESSION

#
# Configuration fixtures
#
@pytest.fixture
def customer_id():
    return _IX_API_CONFIG.customer_id

@pytest.fixture
def customer(session, customer_id):
    _, customer = crm.customers_retrieve(session, customer_id)
    return customer

@pytest.fixture
def customer_implementation_contact_id():
    return _IX_API_CONFIG.customer_implementation_contact_id


@pytest.fixture
def customer_implementation_contact(
        session,
        customer_implementation_contact_id,
    ):
    _, contact = crm.contacts_retrieve(
        session, customer_implementation_contact_id)
    return contact

@pytest.fixture
def customer_billing_contact_id():
    return _IX_API_CONFIG.customer_billing_contact_id

@pytest.fixture
def customer_billing_contact(
        session,
        customer_billing_contact_id,
    ):
    _, contact = crm.contacts_retrieve(
        session, customer_billing_contact_id)
    return contact


@pytest.fixture
def subcustomer_id():
    return _IX_API_CONFIG.subcustomer_id

@pytest.fixture
def subcustomer(session, subcustomer_id):
    _, customer = crm.customers_retrieve(
        session, subcustomer_id)
    return customer

@pytest.fixture
def subcustomer_noc_contact_id():
    return _IX_API_CONFIG.subcustomer_noc_contact_id

@pytest.fixture
def subcustomer_noc_contact(
        session,
        subcustomer_noc_contact_id,
    ):
    _, contact = crm.contacts_retrieve(
        session, subcustomer_noc_contact_id)
    return contact

@pytest.fixture
def subcustomer_implementation_contact_id():
    return _IX_API_CONFIG.subcustomer_implementation_contact_id

@pytest.fixture
def subcustomer_implementation_contact(
        session,
        subcustomer_implementation_contact_id,
    ):
    _, contact = crm.contacts_retrieve(
        session, subcustomer_implementation_contact_id)
    return contact

@pytest.fixture
def subcustomer_connection_id():
    return _IX_API_CONFIG.subcustomer_connection_id

@pytest.fixture
def subcustomer_connection(
        session,
        subcustomer_connection_id,
    ):
    _, conn = access.connections_retrieve(
        session, subcustomer_connection_id)
    return conn

@pytest.fixture
def exchange_lan_network_service_id():
    return _IX_API_CONFIG.exchange_lan_network_service_id

@pytest.fixture
def exchange_lan_network_service(
        session,
        exchange_lan_network_service_id,
    ):
    _, exchange_lan = service.network_services_retrieve(
        session, exchange_lan_network_service_id)
    return exchange_lan



def pytest_addoption(parser):
    """
    Add configuration options for the api test suite:
     - api key
     - api secret
     - host
     or as an alternative
     - test-config
    """
    group = parser.getgroup("ixapi")
    group.addoption(
        "--api-key",
        action="store",
        dest="api_key",
        required=False,
        default=None,
        help="The key for accessing the api")
    group.addoption(
        "--noreset",
        action="store",
        required=False,
        default=False,
        help="Do not reset sandbox before running tests")

    group.addoption(
        "--api-secret",
        action="store",
        dest="api_secret",
        required=False,
        default=None,
        help="The secret for accessing the api")

    group.addoption(
        "--api-host",
        dest="api_base_url",
        required=False,
        default="",
        help="The server where the API is hosted")

    group.addoption(
        "--openapi-spec-v1",
        action="store",
        dest="spec_file_v1",
        default="schema/ix-api-schema-v1.json",
        help="The ix-api v1 OpenAPI spec file")

    group.addoption(
        "--openapi-spec-v2",
        action="store",
        dest="spec_file_v2",
        default="schema/ix-api-schema-v2.json",
        help="The ix-api v1 OpenAPI spec file")

    group.addoption(
        "--test-config",
        action="store",
        dest="test_config",
        required=False,
        default=None,
        help="A path or uri to a test suite configuration json")


def pytest_sessionstart(session):
    """Before running tests"""
    trigger_url = _IX_API_CONFIG.reset_trigger_url

    if _ARGS.noreset:
        return

    print_f("<red>Triggering state reset...</red>")
    res = requests.post(trigger_url, timeout=300.0)
    if res.status_code != 200:
        print_f("<red><bold>ERROR: {}</bold></red>", res.text)
        pytest.exit()
    else:
        print_f("<green>State reset successful.</green>")




def pytest_configure(config):
    """
    Setup pytest and configure ix-api environment
    """
    args = config.option
    if not args.test_config:
        if not args.api_key or \
            not args.api_secret or \
            not args.host:
                pytest.exit("Missing IX-API configuration", 4)

    global _ARGS
    _ARGS = args

    # Load config file if available
    if args.test_config:
        config = load_json_config(args.test_config)
        args = Namespace(**config)

    # Prepare args
    if args.api_base_url.endswith("/"):
        # Remove trailing slash
        args.api_base_url = args.api_base_url[:-1]
    if not args.api_base_url.startswith("http"):
        # Assume http
        args.api_base_url = "http://" + args.api_base_url

    print_f("")
    print_f("<orange>IX-API TEST SUITE</orange>")
    print("")
    print_f("<b>Configuration:</b>")
    print(" - host: \t\t {}".format(args.api_base_url))
    print(" - api key:  \t\t {}".format(args.api_key))
    print(" - api secret: \t\t *SET*")
    print("")

    global _IX_API_CONFIG
    _IX_API_CONFIG = args

    # Inform our code that we are running in test suite mode
    setattr(sys, "_is_ixapi_test_suite", True)
