
import pytest

from ixapi_client.v1.resources import (
    crm,
)

@pytest.mark.skip
def test_unique(session, customer):
    """
    ...
    """
    res, contact = crm.contacts_create(session, {
        "type": "noc",
        "email": "foo@bar.com",
        "telephone": "123",
        "managing_customer": customer['id'],
        "consuming_customer": customer['id'],
    })
    assert res.ok

    # However, let's try to reuse the email
    res, _ = crm.contacts_create(session, {
        "type": "noc",
        "email": "foo@bar.com",
        "telephone": "123",
        "managing_customer": customer['id'],
        "consuming_customer": customer['id'],
    })
    assert not res.ok,  \
        "We expect email addresses to be unique"
