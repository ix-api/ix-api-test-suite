
"""
Test scenario: A subcustomer wants to be connected to
the exchange lan.
"""

import random
import secrets

from ixapi_client.v1.resources import (
    crm,
    ipam,
    service,
    access,
)


def _require_contacts(contacts, required_contact_types):
    """
    From a list of contacts, get only the contacts
    required by type.
    """
    return [c for c in contacts
            if c["type"] in required_contact_types]


def _configure_routeserver_feature(session, request):
    """Configure a route server feature"""
    asn = request["asn"]
    feature = request["network_feature"]
    address_families = feature["address_families"]

    required_contacts = _require_contacts(
        request["contacts"],
        feature["required_contact_types"])

    # Build request
    config_request = {
        "type": "route_server",
        "network_feature": feature["id"],
        "network_service_config": request["network_service_config"]["id"],
        "consuming_customer": request["consuming_customer"]["id"],
        "managing_customer": request["managing_customer"]["id"],
        "session_mode": "public",
        "bgp_session_type": "passive",
        "asn": asn,
        "contacts": [c["id"] for c in required_contacts],
    }

    # Address family specifics
    if "af_inet" in address_families:
        config_request["as_set_v4"] = "AS-DEMO@PRIVATE"
    if "af_inet6" in address_families:
        config_request["as_set_v6"] = "AS-DEMO@PRIVATE"

    res, config = access.network_feature_configs_create(
        session,
        config_request)
    assert res.ok

    return config


def _register_mac_address(session, request):
    """Add a mac address"""
    address = ":".join(secrets.token_hex(1) for _ in range(6))
    address = request.get("address", address)

    res, mac = ipam.macs_create(session, {
        "managing_customer": request["managing_customer"]["id"],
        "consuming_customer": request["consuming_customer"]["id"],
        "address": address,
    })
    assert res.ok

    return mac


def test_configure_peering_for_subcustomer(
        session,
        customer,
        customer_billing_contact,
        subcustomer,
        subcustomer_implementation_contact,
        subcustomer_noc_contact,
        subcustomer_connection,
        exchange_lan_network_service,
    ):
    """
    Configure an exchange lan network service config and
    configure the required network features.
    """
    print()
    print("Reseller customer: {}".format(customer["name"]))
    print("Configuring peering for subcustomer: {}, id: {}".format(
        subcustomer["name"], subcustomer["id"]))

    # Contacts set
    contacts = [
        customer_billing_contact,
        subcustomer_implementation_contact,
        subcustomer_noc_contact,
    ]

    subcustomer_asn = random.randint(64512, 65534)

    # Get required contacts
    required_contact_types = \
        exchange_lan_network_service["required_contact_types"]
    network_service_contacts = _require_contacts(
        contacts, required_contact_types)

    print("Required contact types: {}".format(required_contact_types))

    # Register a mac address
    mac = _register_mac_address(session, {
        "managing_customer": customer,
        "consuming_customer": subcustomer,
    })

    res, exchange_lan_config = access.network_service_configs_create(session, {
        "type": "exchange_lan",
        "managing_customer": customer["id"],
        "consuming_customer": subcustomer["id"],
        "network_service": exchange_lan_network_service["id"],
        "connection": subcustomer_connection["id"],
        "outer_vlan": [[0, 4096]],
        "contacts": [c["id"] for c in network_service_contacts],
        "macs": [mac["id"]],
        "asns": [subcustomer_asn],
    })
    assert res.ok


    # Load network features, see which are required
    network_features = [
        service.network_features_retrieve(session, feature_id)[1]
        for feature_id in exchange_lan_network_service["network_features"]
    ]
    required_network_features = [
        feature for feature in network_features if feature["required"]
    ]

    print("Configuring required network features: {}".format(
        [f["name"] for f in required_network_features]))

    feature_configs = []
    for feature in required_network_features:
        if feature["type"] == "route_server":
            feature_config = _configure_routeserver_feature(session, {
                "asn": subcustomer_asn,
                "managing_customer": customer,
                "consuming_customer": subcustomer,
                "network_feature": feature,
                "network_service_config": exchange_lan_config,
                "contacts": contacts,
            })
        else:
            raise NotImplemented(
                "No configuration strategy for: {}".format(feature["type"]))

        feature_configs.append(feature_config) 
    # Print summary
    print("Created {} feature configs.".format(len(feature_configs)))

    # Reload exchange lan config
    _, exchange_lan_config = access.network_service_configs_retrieve(
        session, exchange_lan_config["id"])

    print("Created network service config.")
    print("Assigned ip addresses:")

    ips = [ipam.ips_retrieve(session, ipid, validate_response=False)[1]
           for ipid in exchange_lan_config["ips"]]

    for ip in ips:
        print(" - {}/{}".format(ip["address"], ip["prefix_length"]))

