
"""Text/UI utilities"""

import inspect

from prompt_toolkit.formatted_text import HTML
from prompt_toolkit import print_formatted_text


def print_f(*args, **kwargs):
    """
    Print "HTML" formatted markup. Args and
    kwargs will be passed to format.

    WARNING: This implementation is unsafe.
    """
    try:
        markup = args[0]
        args = args[1:]
    except IndexError:
        markup = ""
    print_formatted_text(
        HTML(markup.format(*args, **kwargs)))


def print_doc(docstring, *args, **kwargs):
    """
    Print a (formatted) docstring
    """
    print_f(inspect.cleandoc(docstring), *args, **kwargs)

