
"""
Bootstrap the api console
"""

# For convenience:
import sys
from importlib import reload

from requests import exceptions as requests_exceptions

from ixapi_client.v1 import client
from cli.utils import schema, loaders
from cli.utils.text import print_f


def init(args):
    """Initialize the api shell"""
    return init_v1(args)


def init_v1(args):
    """Initialize shell for use with v1 api"""
    def dial():
        """Create new client session"""
        return client.dial(args.api_base_url, args.api_key, args.api_secret)

    session = dial()

    # Resources
    resources = loaders.load_package_path("ixapi_client/v1/resources")
    print_f("<cyan><b>Available Resources:</b></cyan>")
    for name, _ in resources.items():
        print(" - {}".format(name))
    print("")

    return {
        "session": session,
        "dial": dial,
        "reload": reload,
        **resources,
        **args.__dict__,
    }

