"""
Launch the testrunner pytest
"""

import subprocess
import sys

def __main__():
    """Run pytest on ixapi_tests"""
    result = subprocess.run([
        "pytest", 
        "-s", "-v",
        *sys.argv[1:],
        "ixapi_tests/v1"
    ])

    sys.exit(result.returncode)


